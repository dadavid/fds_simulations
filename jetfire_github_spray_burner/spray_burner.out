
 Fire Dynamics Simulator

 Current Date     : June 27, 2021  16:21:00
 Revision         : FDS6.7.6-0-g5064c500c-release
 Revision Date    : Thu May 27 12:19:10 2021 -0400
 Compiler         : Intel ifort 2021.1
 Compilation Date : Tue 06/01/2021  11:25 AM

 MPI Enabled;    Number of MPI Processes:       1
 OpenMP Enabled; Number of OpenMP Threads:      4

 MPI version: 3.1
 MPI library version: Intel(R) MPI Library 2021.1 for Windows* OS


 Job TITLE        : Test Simulation of 2 MW Heptane Spray Burner
 Job ID string    : spray_burner


 Grid Dimensions, Mesh     1

   Cells in the X Direction            20
   Cells in the Y Direction            30
   Cells in the Z Direction            40
   Number of Grid Cells             24000


 Physical Dimensions, Mesh     1

   Length (m)                       2.000
   Width  (m)                       3.000
   Height (m)                       4.000
   Initial Time Step (s)            0.079

Total Number of Grid Cells         24000


 Miscellaneous Parameters

   Simulation Start Time (s)          0.0
   Simulation End Time (s)           10.0
   LES Calculation
   Eddy Viscosity:           Deardorff Model (C_DEARDORFF = 0.10)
   Near-wall Eddy Viscosity: WALE Model (C_WALE = 0.60)
   Turbulent Prandtl Number:         0.50
   Turbulent Schmidt Number:         0.50
   Background Pressure (Pa):    101325.00
   Ambient Temperature (C):         20.00


 Background Stratification

      Z (m)     P_0 (Pa)    TMP_0 (C)
   ------------------------------------
        3.95    101278.54     20.00
        3.85    101279.72     20.00
        3.75    101280.89     20.00
        3.65    101282.07     20.00
        3.55    101283.24     20.00
        3.45    101284.42     20.00
        3.35    101285.60     20.00
        3.25    101286.77     20.00
        3.15    101287.95     20.00
        3.05    101289.12     20.00
        2.95    101290.30     20.00
        2.85    101291.47     20.00
        2.75    101292.65     20.00
        2.65    101293.82     20.00
        2.55    101295.00     20.00
        2.45    101296.18     20.00
        2.35    101297.35     20.00
        2.25    101298.53     20.00
        2.15    101299.70     20.00
        2.05    101300.88     20.00
        1.95    101302.06     20.00
        1.85    101303.23     20.00
        1.75    101304.41     20.00
        1.65    101305.58     20.00
        1.55    101306.76     20.00
        1.45    101307.93     20.00
        1.35    101309.11     20.00
        1.25    101310.29     20.00
        1.15    101311.46     20.00
        1.05    101312.64     20.00
        0.95    101313.81     20.00
        0.85    101314.99     20.00
        0.75    101316.17     20.00
        0.65    101317.34     20.00
        0.55    101318.52     20.00
        0.45    101319.69     20.00
        0.35    101320.87     20.00
        0.25    101322.05     20.00
        0.15    101323.22     20.00
        0.05    101324.40     20.00


 Mass Fraction Transformation Matrix to Convert Species Mixtures (Columns) to Primitive Species (Rows)

                         AIR       N-HEPTAN  PRODUCTS
   N-HEPTANE             0.000000  1.000000  0.000000
   NITROGEN              0.767622  0.000000  0.719470
   OXYGEN                0.232378  0.000000  0.000000
   CARBON DIOXIDE        0.000000  0.000000  0.189442
   WATER VAPOR           0.000000  0.000000  0.090147
   CARBON MONOXIDE       0.000000  0.000000  0.000000
   SOOT                  0.000000  0.000000  0.000941
   HYDROGEN              0.000000  0.000000  0.000000
   HYDROGEN CYANIDE      0.000000  0.000000  0.000000


 Primitive Species Information

   N-HEPTANE
   Gas Species
   Molecular Weight (g/mol)           100.20194
   Ambient Density (kg/m^3)            4.166
   Enthalpy of Formation (J/kg)       -1.87E+06

   NITROGEN
   Gas Species
   Molecular Weight (g/mol)            28.01340
   Ambient Density (kg/m^3)            1.165
   Enthalpy of Formation (J/kg)        0.00E+00

   OXYGEN
   Gas Species
   Molecular Weight (g/mol)            31.99880
   Ambient Density (kg/m^3)            1.330
   Enthalpy of Formation (J/kg)        0.00E+00

   CARBON DIOXIDE
   Gas Species
   Molecular Weight (g/mol)            44.00950
   Ambient Density (kg/m^3)            1.830
   Enthalpy of Formation (J/kg)       -8.94E+06

   WATER VAPOR
   Gas Species
   Molecular Weight (g/mol)            18.01528
   Ambient Density (kg/m^3)            0.749
   Enthalpy of Formation (J/kg)       -1.34E+07

   CARBON MONOXIDE
   Gas Species
   Molecular Weight (g/mol)            28.01010
   Ambient Density (kg/m^3)            1.164
   Enthalpy of Formation (J/kg)       -3.95E+06

   SOOT
   Gas Species
   Molecular Weight (g/mol)            10.91042
   Ambient Density (kg/m^3)            0.454
   Enthalpy of Formation (J/kg)        0.00E+00

   HYDROGEN
   Gas Species
   Molecular Weight (g/mol)             2.01588
   Ambient Density (kg/m^3)            0.084
   Enthalpy of Formation (J/kg)        0.00E+00

   HYDROGEN CYANIDE
   Gas Species
   Molecular Weight (g/mol)            27.02534
   Ambient Density (kg/m^3)            1.123
   Enthalpy of Formation (J/kg)        4.92E+06


 Tracked (Lumped) Species Information

   AIR
   Molecular Weight (g/mol)            28.84834
   Ambient Density (kg/m^3)            1.199
   Initial Mass Fraction               1.000
   Enthalpy of Formation (J/kg)      0.00E+00

   Sub Species                    Mass Fraction     Mole Fraction
   NITROGEN                       7.676220E-01      7.905009E-01
   OXYGEN                         2.323780E-01      2.094991E-01
   CARBON DIOXIDE                 0.000000E+00      0.000000E+00
   WATER VAPOR                    0.000000E+00      0.000000E+00
 
     Viscosity (kg/m/s) Ambient,  293 K:  1.80E-05
                                  500 K:  2.62E-05
                                 1000 K:  4.13E-05
                                 1500 K:  5.37E-05
   Therm. Cond. (W/m/K) Ambient,  293 K:  2.56E-02
                                  500 K:  3.82E-02
                                 1000 K:  6.67E-02
                                 1500 K:  9.22E-02
        Enthalpy (J/kg) Ambient,  293 K: -5.06E+03
                                  500 K:  2.06E+05
                                 1000 K:  7.53E+05
                                 1500 K:  1.35E+06
    Spec. Heat (J/kg/K) Ambient,  293 K:  1.01E+03
                                  500 K:  1.04E+03
                                 1000 K:  1.15E+03
                                 1500 K:  1.22E+03
   Diff. Coeff. (m^2/s) Ambient,  293 K:  1.99E-05
                                  500 K:  4.95E-05
                                 1000 K:  1.58E-04
                                 1500 K:  3.09E-04

   N-HEPTANE
   Molecular Weight (g/mol)           100.20194
   Ambient Density (kg/m^3)            4.166
   Initial Mass Fraction               0.000
   Enthalpy of Formation (J/kg)     -1.87E+06

   Sub Species                    Mass Fraction     Mole Fraction
   N-HEPTANE                      1.000000E+00      1.000000E+00
 
     Viscosity (kg/m/s) Ambient,  293 K:  1.54E-05
                                  500 K:  2.45E-05
                                 1000 K:  4.11E-05
                                 1500 K:  5.41E-05
   Therm. Cond. (W/m/K) Ambient,  293 K:  3.02E-02
                                  500 K:  7.42E-02
                                 1000 K:  1.89E-01
                                 1500 K:  2.89E-01
        Enthalpy (J/kg) Ambient,  293 K: -1.88E+06
                                  500 K: -1.45E+06
                                 1000 K:  1.74E+05
                                 1500 K:  2.25E+06
    Spec. Heat (J/kg/K) Ambient,  293 K:  1.63E+03
                                  500 K:  2.52E+03
                                 1000 K:  3.81E+03
                                 1500 K:  4.43E+03
   Diff. Coeff. (m^2/s) Ambient,  293 K:  1.10E-05
                                  500 K:  2.83E-05
                                 1000 K:  9.18E-05
                                 1500 K:  1.80E-04
 
   Liq. Enthalpy (J/kg)     Melt  183 K: -2.47E+06
                                  277 K: -2.29E+06
                            Boil  372 K: -2.06E+06
 
   Liq. Spec. Heat (J/kg/K) Melt  183 K:  2.02E+03
                                  277 K:  2.17E+03
                            Boil  372 K:  2.56E+03
 
   Heat of Vapor. (J/kg)    Melt  183 K:  4.41E+05
                                  277 K:  3.78E+05
                            Boil  372 K:  3.22E+05

   PRODUCTS
   Molecular Weight (g/mol)            28.50807
   Ambient Density (kg/m^3)            1.185
   Initial Mass Fraction               0.000
   Enthalpy of Formation (J/kg)     -2.90E+06

   Sub Species                    Mass Fraction     Mole Fraction
   NITROGEN                       7.194696E-01      7.321741E-01
   CARBON DIOXIDE                 1.894425E-01      1.227153E-01
   WATER VAPOR                    9.014695E-02      1.426520E-01
   CARBON MONOXIDE                0.000000E+00      0.000000E+00
   SOOT                           9.409391E-04      2.458599E-03
   HYDROGEN CYANIDE               0.000000E+00      0.000000E+00
 
     Viscosity (kg/m/s) Ambient,  293 K:  1.62E-05
                                  500 K:  2.40E-05
                                 1000 K:  3.90E-05
                                 1500 K:  5.14E-05
   Therm. Cond. (W/m/K) Ambient,  293 K:  2.34E-02
                                  500 K:  3.62E-02
                                 1000 K:  6.68E-02
                                 1500 K:  9.55E-02
        Enthalpy (J/kg) Ambient,  293 K: -2.91E+06
                                  500 K: -2.68E+06
                                 1000 K: -2.08E+06
                                 1500 K: -1.41E+06
    Spec. Heat (J/kg/K) Ambient,  293 K:  1.08E+03
                                  500 K:  1.13E+03
                                 1000 K:  1.28E+03
                                 1500 K:  1.38E+03
   Diff. Coeff. (m^2/s) Ambient,  293 K:  1.95E-05
                                  500 K:  4.93E-05
                                 1000 K:  1.59E-04
                                 1500 K:  3.12E-04


 Gas Phase Reaction Information

   Fuel                                           Heat of Combustion (kJ/kg)
   N-HEPTANE                                                      44419.7293

   Stoichiometry

   Primitive Species Stoich. Coeff.
   Species ID                                                          Molar
   N-HEPTANE                                                       -1.000000
   OXYGEN                                                         -10.872571
   CARBON DIOXIDE                                                   6.876015
   WATER VAPOR                                                      7.993112
   SOOT                                                             0.137761

   Tracked (Lumped) Species Stoich. Coeff.
   Species ID                                             Molar         Mass
   AIR                                               -51.897944   -14.941521
   N-HEPTANE                                          -1.000000    -1.000000
   PRODUCTS                                           56.032261    15.941521

   Reaction Kinetics

   Arrhenius Parameters
   Pre-exponential:    Infinite
   Activation Energy:  N/A

   ODE Solver:  EXPLICIT EULER
   Extinction Model:  EXTINCTION 2
   Auto-Ignition Temperature (K):       0.0
   Critical Flame Temperature (K):   1770.2


 Material Information

   1 STEEL                                                       
     A242 Steel
     Emissivity                      0.900
     Density (kg/m3)                7850.0
     Specific Heat (kJ/kg/K)      4.50E-01
     Conductivity (W/m/K)          4.80E+01

   2 GYPSUM                                                      
     Emissivity                      0.900
     Density (kg/m3)                 790.0
     Specific Heat (kJ/kg/K)      9.00E-01
     Conductivity (W/m/K)          1.60E-01


 Surface Conditions

   0 INERT

   1 STEEL SHEET
     Material List
          1  STEEL
     Layer  1
        Thickness   (m):  0.00300
        Density (kg/m3):  7850.00
        STEEL, Mass fraction:    1.00
     Total surface density    23.550 kg/m2
     Reaction products considered from the first  0.50 layers.
     Solid Phase Node, Layer, Coordinates(m):
                    0      1       0.0000000
                    1      1       0.0030000
     Exposed Backing

   2 GYPSUM BOARD (DEFAULT)
     Material List
          1  GYPSUM
     Layer  1
        Thickness   (m):  0.02540
        Density (kg/m3):   790.00
        GYPSUM, Mass fraction:    1.00
     Total surface density    20.066 kg/m2
     Reaction products considered from the first  0.50 layers.
     Solid Phase Node, Layer, Coordinates(m):
                    0      1       0.0000000
                    1      1       0.0004097
                    2      1       0.0012290
                    3      1       0.0028677
                    4      1       0.0061452
                    5      1       0.0127000
                    6      1       0.0192548
                    7      1       0.0225323
                    8      1       0.0241710
                    9      1       0.0249903
                   10      1       0.0254000
     Exposed Backing

   3 OPEN
     Passive Vent to Atmosphere

   4 MIRROR
     Symmetry Plane

   5 INTERPOLATED

   6 PERIODIC

   7 HVAC

   8 MASSLESS TRACER

   9 DROPLET

  10 EVACUATION_OUTFLOW
     Normal Velocity (m/s)         0.000

  11 MASSLESS TARGET

  12 PERIODIC FLOW ONLY


 Device Properties

   1 nozzle
     Flow Rate (L/min)               1.97
     Particle Class              heptane droplets
     Smokeview ID                nozzle


 Cut-off Density and Temperature

    Minimum Temperature:   -90.6 C
    Maximum Temperature:  2726.8 C
    Minimum Density:  1.199E-01 kg/m3
    Maximum Density:  9.281E+00 kg/m3


 Device Coordinates

     1 Coords:      4.000000     -0.300000      0.500000, Make: nozzle, ID: nozzle_1, Quantity: TIME
     2 Coords:      4.000000      0.300000      0.500000, Make: nozzle, ID: nozzle_2, Quantity: TIME


 Slice File Information, Mesh     1

   Sampling Interval (s)             0.010

   1 Nodes:  10  10   0  30   0  40, Quantity: TEMPERATURE
   2 Nodes:  10  10   0  30   0  40, Quantity: U-VELOCITY
   3 Nodes:  10  10   0  30   0  40, Quantity: V-VELOCITY
   4 Nodes:  10  10   0  30   0  40, Quantity: W-VELOCITY
   5 Nodes:  10  10   0  30   0  40, Quantity: HRRPUV


 Boundary File Information

   Sampling Interval (s)             0.020

   1 Quantity: WALL TEMPERATURE
   2 Quantity: heptane droplets CPUA
   3 Quantity: heptane droplets MPUA
   4 Quantity: heptane droplets AMPUA


 Radiation Model Information

   Number of control angles  104
   Time step increment         3
   Angle increment             5
   Theta band N_phi   Solid angle
     1:        4     0.120
     2:       12     0.114
     3:       16     0.127
     4:       20     0.120
     5:       20     0.120
     6:       16     0.127
     7:       12     0.114
     8:        4     0.120
   Using gray gas absorption.
   Mean beam length  0.100 m
 
 

 Run Time Diagnostics

       Time Step       1   June 27, 2021  16:21:00
       Step Size:    0.791E-01 s, Total Time:       0.08 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.33E-02 on Mesh   1 at (  16  23   1)
       Maximum Pressure Error:  0.36E-05 on Mesh   1 at (   4   1  40)
       ---------------------------------------------------------------
       Max CFL number:  0.20E-01 at (  16,  18,  25)
       Max divergence:  0.12E-05 at (  17,  11,  15)
       Min divergence: -0.13E-05 at (  17,   8,  35)
       Max VN number:   0.21E-01 at (  19,  15,   1)
 
       Time Step       2   June 27, 2021  16:21:00
       Step Size:    0.791E-01 s, Total Time:       0.16 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.14E-02 on Mesh   1 at (  16  13   1)
       Maximum Pressure Error:  0.14E-05 on Mesh   1 at (  16   6   2)
       ---------------------------------------------------------------
       Max CFL number:  0.20E-01 at (  16,  18,  25)
       Max divergence:  0.12E-05 at (  17,  11,  15)
       Min divergence: -0.13E-05 at (  17,   8,  35)
       Max VN number:   0.21E-01 at (  19,  15,   1)
 
       Time Step       3   June 27, 2021  16:21:00
       Step Size:    0.791E-01 s, Total Time:       0.24 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.17E-02 on Mesh   1 at (  16  23   1)
       Maximum Pressure Error:  0.12E-05 on Mesh   1 at (  15  23   2)
       ---------------------------------------------------------------
       Max CFL number:  0.20E-01 at (  16,  18,  25)
       Max divergence:  0.32E-04 at (  10,  12,   1)
       Min divergence: -0.67E-05 at (  10,  19,   3)
       Max VN number:   0.21E-01 at (  19,  15,   1)
       No. of Lagrangian Particles:           790
 
       Time Step       4   June 27, 2021  16:21:00
       Step Size:    0.791E-01 s, Total Time:       0.32 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.89E-03 on Mesh   1 at (  16  23   1)
       Maximum Pressure Error:  0.14E-04 on Mesh   1 at (  11  17   1)
       ---------------------------------------------------------------
       Max CFL number:  0.20E-01 at (  16,  18,  25)
       Max divergence:  0.13E-01 at (  10,  12,   1)
       Min divergence: -0.70E-05 at (   8,  11,   2)
       Max VN number:   0.21E-01 at (  19,  15,   1)
       No. of Lagrangian Particles:          1580
 
       Time Step       5   June 27, 2021  16:21:00
       Step Size:    0.791E-01 s, Total Time:       0.40 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.77E-03 on Mesh   1 at (  16  16   1)
       Maximum Pressure Error:  0.18E-02 on Mesh   1 at (  10  12   1)
       ---------------------------------------------------------------
       Max CFL number:  0.20E-01 at (  16,  18,  25)
       Max divergence:  0.40E-01 at (  11,  13,   1)
       Min divergence: -0.13E-05 at (  17,   8,  35)
       Max VN number:   0.21E-01 at (  19,  15,   1)
       No. of Lagrangian Particles:          2369
 
       Time Step       6   June 27, 2021  16:21:00
       Step Size:    0.791E-01 s, Total Time:       0.47 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.64E-03 on Mesh   1 at (   6  19   1)
       Maximum Pressure Error:  0.69E-02 on Mesh   1 at (  10  18   1)
       ---------------------------------------------------------------
       Max CFL number:  0.20E-01 at (  16,  18,  25)
       Max divergence:  0.94E-01 at (  11,  12,   1)
       Min divergence: -0.13E-05 at (  17,   8,  35)
       Max VN number:   0.21E-01 at (  19,  15,   1)
       No. of Lagrangian Particles:          3159
 
       Time Step       7   June 27, 2021  16:21:00
       Step Size:    0.791E-01 s, Total Time:       0.55 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.69E-03 on Mesh   1 at (   6  19   1)
       Maximum Pressure Error:  0.18E-01 on Mesh   1 at (  11  19   1)
       ---------------------------------------------------------------
       Max CFL number:  0.21E-01 at (  11,  18,   1)
       Max divergence:  0.16E+00 at (  11,  12,   1)
       Min divergence: -0.13E-05 at (  17,   8,  35)
       Max VN number:   0.20E-01 at (  19,  15,   1)
       No. of Lagrangian Particles:          3947
 
       Time Step       8   June 27, 2021  16:21:01
       Step Size:    0.791E-01 s, Total Time:       0.63 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.55E-03 on Mesh   1 at (   6  17   1)
       Maximum Pressure Error:  0.48E-01 on Mesh   1 at (  10  13   1)
       ---------------------------------------------------------------
       Max CFL number:  0.27E-01 at (  11,  18,   1)
       Max divergence:  0.24E+00 at (  11,  19,   1)
       Min divergence: -0.13E-05 at (  17,   8,  35)
       Max VN number:   0.20E-01 at (  19,  15,   1)
       No. of Lagrangian Particles:          4727
 
       Time Step       9   June 27, 2021  16:21:01
       Step Size:    0.791E-01 s, Total Time:       0.71 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.54E-03 on Mesh   1 at (   6  14   1)
       Maximum Pressure Error:  0.60E-01 on Mesh   1 at (  11  19   1)
       ---------------------------------------------------------------
       Max CFL number:  0.35E-01 at (  11,  19,   1)
       Max divergence:  0.30E+00 at (  11,  18,   1)
       Min divergence: -0.13E-05 at (  17,   8,  35)
       Max VN number:   0.19E-01 at (  19,  15,   1)
       No. of Lagrangian Particles:          5507
 
       Time Step      10   June 27, 2021  16:21:01
       Step Size:    0.791E-01 s, Total Time:       0.79 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.32E-03 on Mesh   1 at (   6  14   1)
       Maximum Pressure Error:  0.68E-01 on Mesh   1 at (  10  13   1)
       ---------------------------------------------------------------
       Max CFL number:  0.44E-01 at (  11,  19,   1)
       Max divergence:  0.41E+00 at (  11,  18,   1)
       Min divergence: -0.13E-05 at (  17,   8,  35)
       Max VN number:   0.18E-01 at (  19,  15,   1)
       No. of Lagrangian Particles:          6276
 
       Time Step      20   June 27, 2021  16:21:03
       Step Size:    0.791E-01 s, Total Time:       1.58 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.45E-02 on Mesh   1 at (   6  18   1)
       Maximum Pressure Error:  0.17E+02 on Mesh   1 at (  10  18   1)
       ---------------------------------------------------------------
       Max CFL number:  0.93E+00 at (  11,  12,   2)
       Max divergence:  0.14E+01 at (  11,  18,   1)
       Min divergence: -0.21E-05 at (   6,  14,   4)
       Max VN number:   0.31E+00 at (  11,  12,   3)
       No. of Lagrangian Particles:         12259
 
       Time Step      30   June 27, 2021  16:21:05
       Step Size:    0.309E-01 s, Total Time:       2.00 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.25E-02 on Mesh   1 at (  16  13   1)
       Maximum Pressure Error:  0.32E+02 on Mesh   1 at (  11  12   8)
       ---------------------------------------------------------------
       Max CFL number:  0.93E+00 at (  11,  13,   7)
       Max divergence:  0.21E+01 at (  10,  13,   1)
       Min divergence: -0.12E+01 at (  11,  12,   8)
       Max VN number:   0.30E+00 at (  11,  12,   7)
       No. of Lagrangian Particles:         15593
 
       Time Step      40   June 27, 2021  16:21:07
       Step Size:    0.243E-01 s, Total Time:       2.26 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.14E-02 on Mesh   1 at (   6  12   1)
       Maximum Pressure Error:  0.34E+02 on Mesh   1 at (  10  19  12)
       ---------------------------------------------------------------
       Max CFL number:  0.10E+01 at (  10,  12,  11)
       Max divergence:  0.24E+01 at (  11,  12,   1)
       Min divergence: -0.11E+01 at (  10,  13,   8)
       Max VN number:   0.32E+00 at (  11,  13,  10)
       No. of Lagrangian Particles:         17403
 
       Time Step      50   June 27, 2021  16:21:08
       Step Size:    0.212E-01 s, Total Time:       2.48 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.49E-03 on Mesh   1 at (   6  19   1)
       Maximum Pressure Error:  0.37E+02 on Mesh   1 at (  11  13   1)
       ---------------------------------------------------------------
       Max CFL number:  0.99E+00 at (  11,  18,  15)
       Max divergence:  0.28E+01 at (  11,  19,   1)
       Min divergence: -0.11E+01 at (  10,  18,   9)
       Max VN number:   0.32E+00 at (  11,  18,  14)
       No. of Lagrangian Particles:         18858
 
       Time Step      60   June 27, 2021  16:21:10
       Step Size:    0.187E-01 s, Total Time:       2.67 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.66E-03 on Mesh   1 at (  16  15   1)
       Maximum Pressure Error:  0.11E+03 on Mesh   1 at (  10  13   1)
       ---------------------------------------------------------------
       Max CFL number:  0.94E+00 at (  11,  13,  19)
       Max divergence:  0.31E+01 at (  10,  17,   1)
       Min divergence: -0.10E+01 at (  10,  18,  11)
       Max VN number:   0.33E+00 at (  11,  18,  19)
       No. of Lagrangian Particles:         20152
 
       Time Step      70   June 27, 2021  16:21:12
       Step Size:    0.167E-01 s, Total Time:       2.85 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.39E-02 on Mesh   1 at (  16  17   1)
       Maximum Pressure Error:  0.13E+03 on Mesh   1 at (  10  18   1)
       ---------------------------------------------------------------
       Max CFL number:  0.90E+00 at (  11,  14,  16)
       Max divergence:  0.33E+01 at (  11,  13,   1)
       Min divergence: -0.12E+01 at (  10,  13,   9)
       Max VN number:   0.29E+00 at (  11,  14,  16)
       No. of Lagrangian Particles:         21372
 
       Time Step      80   June 27, 2021  16:21:14
       Step Size:    0.167E-01 s, Total Time:       3.02 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.73E-02 on Mesh   1 at (   6  18   1)
       Maximum Pressure Error:  0.93E+03 on Mesh   1 at (  10  18   1)
       ---------------------------------------------------------------
       Max CFL number:  0.95E+00 at (  11,  17,  20)
       Max divergence:  0.73E+01 at (  10,  13,   1)
       Min divergence: -0.15E+01 at (  10,  18,  11)
       Max VN number:   0.32E+00 at (  11,  14,  21)
       No. of Lagrangian Particles:         22450
 
       Time Step      90   June 27, 2021  16:21:16
       Step Size:    0.167E-01 s, Total Time:       3.18 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.98E-02 on Mesh   1 at (  16  18   1)
       Maximum Pressure Error:  0.55E+03 on Mesh   1 at (  10  13   1)
       ---------------------------------------------------------------
       Max CFL number:  0.96E+00 at (  10,  14,  27)
       Max divergence:  0.58E+01 at (  10,  13,   1)
       Min divergence: -0.14E+01 at (  10,  13,  11)
       Max VN number:   0.32E+00 at (  10,  17,  27)
       No. of Lagrangian Particles:         23577
 
       Time Step     100   June 27, 2021  16:21:18
       Step Size:    0.147E-01 s, Total Time:       3.34 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.45E-02 on Mesh   1 at (   6  18   1)
       Maximum Pressure Error:  0.90E+03 on Mesh   1 at (  10  18   1)
       ---------------------------------------------------------------
       Max CFL number:  0.87E+00 at (  11,  14,  32)
       Max divergence:  0.72E+01 at (  10,  14,   1)
       Min divergence: -0.17E+01 at (  10,  18,   1)
       Max VN number:   0.33E+00 at (  10,  17,  32)
       No. of Lagrangian Particles:         24571
 
       Time Step     200   June 27, 2021  16:21:39
       Step Size:    0.913E-02 s, Total Time:       4.42 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.21E-01 on Mesh   1 at (   6  15   1)
       Maximum Pressure Error:  0.18E+04 on Mesh   1 at (  10  13   1)
       ---------------------------------------------------------------
       Max CFL number:  0.89E+00 at (  11,  15,  25)
       Max divergence:  0.79E+01 at (  10,  13,   1)
       Min divergence: -0.31E+01 at (  11,  13,  10)
       Max VN number:   0.23E+00 at (  12,  15,  25)
       No. of Lagrangian Particles:         30764
 
       Time Step     300   June 27, 2021  16:22:02
       Step Size:    0.871E-02 s, Total Time:       5.35 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.13E-01 on Mesh   1 at (   6  13   1)
       Maximum Pressure Error:  0.17E+04 on Mesh   1 at (  10  19   1)
       ---------------------------------------------------------------
       Max CFL number:  0.85E+00 at (   9,  15,  28)
       Max divergence:  0.15E+02 at (  10,  13,   1)
       Min divergence: -0.30E+01 at (  10,  17,  14)
       Max VN number:   0.24E+00 at (  12,  15,  16)
       No. of Lagrangian Particles:         34988
 
       Time Step     400   June 27, 2021  16:22:26
       Step Size:    0.840E-02 s, Total Time:       6.25 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.13E-01 on Mesh   1 at (   6  20   1)
       Maximum Pressure Error:  0.18E+04 on Mesh   1 at (  11  13   1)
       ---------------------------------------------------------------
       Max CFL number:  0.95E+00 at (   9,  17,  25)
       Max divergence:  0.14E+02 at (  11,  16,   1)
       Min divergence: -0.32E+01 at (  11,  15,  21)
       Max VN number:   0.24E+00 at (   9,  17,  25)
       No. of Lagrangian Particles:         38635
 
       Time Step     500   June 27, 2021  16:22:51
       Step Size:    0.816E-02 s, Total Time:       7.06 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.91E-02 on Mesh   1 at (   6  15   1)
       Maximum Pressure Error:  0.12E+04 on Mesh   1 at (  12  13   1)
       ---------------------------------------------------------------
       Max CFL number:  0.81E+00 at (  13,  18,  38)
       Max divergence:  0.11E+02 at (  10,  11,   1)
       Min divergence: -0.36E+01 at (  11,  18,  15)
       Max VN number:   0.23E+00 at (  10,  12,  22)
       No. of Lagrangian Particles:         41761
 
       Time Step     600   June 27, 2021  16:23:17
       Step Size:    0.712E-02 s, Total Time:       7.91 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.20E-01 on Mesh   1 at (   6  12   1)
       Maximum Pressure Error:  0.17E+04 on Mesh   1 at (   7  15  22)
       ---------------------------------------------------------------
       Max CFL number:  0.92E+00 at (   7,  15,  22)
       Max divergence:  0.95E+01 at (   9,  17,   1)
       Min divergence: -0.49E+01 at (   7,  15,  21)
       Max VN number:   0.30E+00 at (   7,  15,  20)
       No. of Lagrangian Particles:         44024
 
       Time Step     700   June 27, 2021  16:23:43
       Step Size:    0.866E-02 s, Total Time:       8.64 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.16E-01 on Mesh   1 at (  16  13   1)
       Maximum Pressure Error:  0.10E+04 on Mesh   1 at (  10  14   1)
       ---------------------------------------------------------------
       Max CFL number:  0.83E+00 at (  12,  20,  26)
       Max divergence:  0.12E+02 at (   9,  12,   1)
       Min divergence: -0.43E+01 at (  10,  17,  15)
       Max VN number:   0.24E+00 at (  12,  20,  27)
       No. of Lagrangian Particles:         45078
 
       Time Step     800   June 27, 2021  16:24:09
       Step Size:    0.767E-02 s, Total Time:       9.42 s
       Pressure Iterations:      1
       Maximum Velocity Error:  0.73E-02 on Mesh   1 at (  16  20   1)
       Maximum Pressure Error:  0.19E+04 on Mesh   1 at (  10  12   1)
       ---------------------------------------------------------------
       Max CFL number:  0.89E+00 at (  10,  14,  31)
       Max divergence:  0.14E+02 at (   9,  13,   1)
       Min divergence: -0.35E+01 at (  10,  12,  13)
       Max VN number:   0.19E+00 at (  12,  19,  21)
       No. of Lagrangian Particles:         46078
 
       Time Step     869   June 27, 2021  16:24:28
       Step Size:    0.738E-02 s, Total Time:      10.00 s
       Pressure Iterations:      2
       Maximum Velocity Error:  0.25E-01 on Mesh   1 at (  16  17   1)
       Maximum Pressure Error:  0.82E+03 on Mesh   1 at (  13  15   1)
       ---------------------------------------------------------------
       Max CFL number:  0.84E+00 at (  10,  19,  23)
       Max divergence:  0.16E+02 at (  11,  19,   1)
       Min divergence: -0.44E+01 at (  11,  18,   1)
       Max VN number:   0.29E+00 at (  14,  14,  23)
       No. of Lagrangian Particles:         47013
 


 DEVICE Activation Times

   1  nozzle_1                                                         0.1 s
   2  nozzle_2                                                         0.1 s


 Time Stepping Wall Clock Time (s):      208.272
 Total Elapsed Wall Clock Time (s):      216.030

STOP: FDS completed successfully (CHID: spray_burner)
